package pallares.didac.calculadoraprecios;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String STRING_CANTIDAD = "param1";
    private static final String STRING_PRECIO = "param2";

    private float resultadoSinIva;
    private float resultadoConIva;
    private TextView result;
    private float cantidad;
    private float precio;

    private OnFragmentInteractionListener mListener;


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String str);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    public static BlankFragment newInstance(String param1, String param2) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(STRING_CANTIDAD, param1);
        args.putString(STRING_PRECIO, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cantidad = Float.parseFloat(getArguments().getString(STRING_CANTIDAD));
            precio = Float.parseFloat(getArguments().getString(STRING_PRECIO));
            resultadoSinIva = cantidad * precio;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_blank, container, false);
        result = (TextView) v.findViewById(R.id.fragment_txt_showResult);

        result = (TextView) v.findViewById(R.id.fragment_txt_showResult);
        result.setText(String.valueOf(resultadoSinIva));

        RadioButton btnWithOutIva = (RadioButton) v.findViewById(R.id.fragment_radbtn_withoutIva);
        btnWithOutIva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText(String.valueOf(resultadoSinIva));
            }
        });

        RadioButton btnWithIva = (RadioButton) v.findViewById(R.id.fragment_radbtn_withIva);
        btnWithIva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultadoConIva = (cantidad * precio) + (((cantidad * precio) * 21) / 100);
                ;
                result.setText(String.valueOf(resultadoConIva));
            }
        });

        Button calcular = (Button) v.findViewById((R.id.fragment_btn_calcular));
        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(result.getText().toString());
            }
        });

        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
