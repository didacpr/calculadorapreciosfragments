package pallares.didac.calculadoraprecios;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class secondActivityIVA extends AppCompatActivity implements View.OnClickListener {

    private String cantidad = "";
    private String precio = "";
    private float resultado = 0;
    private TextView txt;

    private String getCantidad() {
        return cantidad;
    }

    private void setCantidad() {
        this.cantidad = getIntent().getExtras().getString("cant");
    }

    private String getPrecio() {
        return precio;
    }

    public void setPrecio() {
        this.precio = getIntent().getExtras().getString("prec");
    }

    private float getResultado() {
        return resultado;
    }

    private void setResultadoWithoutIva() {
        float c = Float.parseFloat(getCantidad());
        float p = Float.parseFloat(getPrecio());
        resultado = c * p;
    }

    private void setResultadoWithIva() {
        float c = Float.parseFloat(getCantidad());
        float p = Float.parseFloat(getPrecio());
        resultado = (c * p) + (((c * p) * 21) / 100);
    }

    public TextView getTxt() {
        return txt;
    }

    public void setTxt() {
        this.txt = (TextView) findViewById(R.id.secondActivity_showResult);
        ;
        txt.setText(String.valueOf(resultado));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("variables", "entra On CREATE");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.segunda_pantalla);

        setCantidad();
        setPrecio();
        setResultadoWithoutIva();
        setTxt();


        Button btnCalc = (Button) findViewById(R.id.secondActivity_calcular);
        btnCalc.setOnClickListener(this);

        RadioButton btnWithIva = (RadioButton) findViewById(R.id.secondActivity_withIva);
        btnWithIva.setOnClickListener(this);

        RadioButton btnWithOutIva = (RadioButton) findViewById(R.id.secondActivity_withoutIva);
        btnWithOutIva.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.secondActivity_calcular:
                Intent ir = new Intent();
                ir.putExtra("result", resultado);
                setResult(this.RESULT_OK, ir);
                finish();
                break;

            case R.id.secondActivity_withIva:
                setResultadoWithIva();
                setTxt();
                break;

            case R.id.secondActivity_withoutIva:
                setResultadoWithoutIva();
                setTxt();
                break;


        }
    }
}
