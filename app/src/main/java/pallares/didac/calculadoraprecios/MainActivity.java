package pallares.didac.calculadoraprecios;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements BlankFragment.OnFragmentInteractionListener {

    private EditText etCant;
    private EditText etPrec;
    private String cantidad;
    private String precio;
    TextView resultado;
    FragmentManager fm;
    FragmentTransaction ft;
    BlankFragment blank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnNext = (Button) findViewById(R.id.mainActivity_btn_moreOptions);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.mainActivity_btn_moreOptions:

                        etCant = (EditText) findViewById(R.id.mainActivity_cantidadValor);
                        cantidad = etCant.getText().toString();
                        if (cantidad.isEmpty()) {
                            etCant.setError("Escribe una cantidad!");
                            break;
                        }

                        etPrec = (EditText) findViewById(R.id.mainActivity_precioValor);
                        precio = etPrec.getText().toString();
                        if (precio.isEmpty()) {
                            etPrec.setError("Escribe un precio!");
                            break;
                        }

                        fm = getSupportFragmentManager();
                        ft = fm.beginTransaction();
                        blank = BlankFragment.newInstance(cantidad, precio);
                        ft.add(R.id.mainActivity_fragmentUbication, blank);
                        ft.commit();

                        break;
                }
            }
        });

    }

    @Override
    public void onFragmentInteraction(String str) {
        resultado = (TextView) findViewById(R.id.mainActivity_Resultado);
        etCant.setText("");
        etPrec.setText("");
        resultado.setText(str);
    }

    private Activity getActivity() {
        return this;
    }
}
