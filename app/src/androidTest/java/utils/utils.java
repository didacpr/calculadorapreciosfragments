package utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public final class utils {

    public static void openWebPage(Activity a, String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(a.getPackageManager()) != null) {
            a.startActivity(intent);
        }
    }

    public static void dialPhoneNumber(Activity a, String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(a.getPackageManager()) != null) {
            a.startActivity(intent);
        }
    }
}
